#!/bin/bash


echo "Content-type: text/html"
echo ""

echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title>Environment Variables</title>'
echo '</head>'
echo '<body>'


# Save the old internal field separator.
  OIFS="$IFS"

# Set the field separator to & and parse the QUERY_STRING at the ampersand.
  IFS="${IFS}&"
  set $QUERY_STRING
  Args="$*"
  IFS="$OIFS"

# Next parse the individual "name=value" tokens.


  for i in $Args ;do

#       Set the field separator to =
        IFS="${OIFS}="
        set $i
        IFS="${OIFS}"

        case $1 in
                # Don't allow "/" changed to " ". Prevent hacker problems.

                brightness) 
			brightness="`echo $2 | sed 's|[\]||g' | sed 's|%20| |g'`"
			v4l2-ctl --set-ctrl=brightness=$2 -d 0
			#echo $?

                       ;;


                # Filter for "/" not applied here
                contrast) 
			contrast="`echo $2 | sed 's|%20| |g'`"
			v4l2-ctl --set-ctrl=contrast=$2 -d 0
			#echo $?
                       ;;


		saturation) 
			saturation="`echo $2 | sed 's|%20| |g'`"
			v4l2-ctl --set-ctrl=saturation=$2 -d 0
			#echo $?
                       ;;


                hue) 
		        hue="${2/\// /}"
			v4l2-ctl --set-ctrl=hue=$2 -d 0
			#echo $?
                       ;;


                gamma) 
		        gamma="${2/\// /}"
			v4l2-ctl --set-ctrl=gamma=$2 -d 0
			#echo $?
                       ;;



                sharpness) 
		        sharpness="${2/\// /}"
			v4l2-ctl --set-ctrl=sharpness=$2 -d 0
			#echo $?
                       ;;



                backlight_compensation) 
		        backlight_compensation="${2/\// /}"
			v4l2-ctl --set-ctrl=backlight_compensation=$2 -d 0
			#echo $?
                       ;;



                white_balance_temperature_auto) 
		        white_balance_temperature_auto="${2/\// /}"
			v4l2-ctl --set-ctrl=white_balance_temperature_auto=$2 -d 0
			#echo $?
                       ;;



                white_balance_temperature) 
		        white_balance_temperature="${2/\// /}"
			v4l2-ctl --set-ctrl=white_balance_temperature=$2 -d 0
			#echo $?
                       ;;



                power_line_frequency) 
		        power_line_frequency="${2/\// /}"
			v4l2-ctl --set-ctrl=power_line_frequency=$2 -d 0
			#echo $?
                       ;;



                exposure_auto) 
		        exposure_auto="${2/\// /}"
			v4l2-ctl --set-ctrl=exposure_auto=$2 -d 0
			#echo $?
                       ;;



                exposure_absolute) 
		        exposure_absolute="${2/\// /}"
			v4l2-ctl --set-ctrl=exposure_absolute=$2 -d 0
			#echo $?
                       ;;



                exposure_auto_priority) 
		        exposure_auto_priority="${2/\// /}"
			v4l2-ctl --set-ctrl=exposure_auto_priority=$2 -d 0
			#echo $?
                       ;;




                *)     echo "<hr>Warning:"\
                            "<br>Unrecognized variable \'$1\' passed by FORM in QUERY_STRING.<hr>"
                       ;;

        esac
  done

  echo 'the Configuration is being set!' 

  echo '<meta http-equiv="refresh" content="1; url='http://localhost/camera.html'" />'

echo '</body>'
echo '</html>'

exit 0
