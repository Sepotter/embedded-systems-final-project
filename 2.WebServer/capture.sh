#!/bin/bash
#This file captures a picture and saves it to captured-pics/ directory.

echo "Content-type: text/html"
echo ""

echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '</head>'
echo '<body>'

echo '<meta http-equiv="refresh" content="1; url='http://localhost/stream.html'" />'


echo '</body>'
echo '</html>'

mkdir -p captured-pics

fswebcam --quiet --save captured-pics/"%Y%m%d%I%M%S".jpg
echo $?

exit 0
