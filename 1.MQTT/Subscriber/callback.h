#include "MQTTClient.h"
#include <unistd.h>
#include <stdio.h>
#include <time.h> 

#define ADDRESS "localhost"

volatile MQTTClient_deliveryToken deliveredtoken;

void delivered(void *context, MQTTClient_deliveryToken dt) {
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    int i;
    char* payloadptr;


    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("   message: ");

    payloadptr = message->payload;

	char pm[50];

	for(i=0; i<message->payloadlen; i++) {
		pm[i] = *payloadptr++;
	}
	
	pm[i+1] = '\0';
	
	printf("%s\n",pm);

	//Open the text file	
	FILE *log = fopen("logfile.txt", "at");

	if (!log) log = fopen("logfile.txt", "wt");
    if (!log) {
        printf("can not open logfile.txt for writing.\n");
    }
	
	//get time
	char buff[20];
    struct tm *sTm;

    time_t now = time (0);
    sTm = gmtime (&now);

    strftime (buff, sizeof(buff), "%Y-%m-%d %H:%M:%S", sTm);
	/************/

	
	// Append new log to the text file	
	char index[500];
	sprintf(index, "%s : %s \n \n", buff, pm);
    
	fputs(index, log);
	/******************/

    fclose(log);

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}


