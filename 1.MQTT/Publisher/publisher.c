#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "MQTTClient.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>


#define ADDRESS     "localhost"
#define CLIENTID    "Pub-Sepehr"

#define PRIORITY "<5>"

int GetCPULoad() {
	int FileHandler;
	char FileBuffer[1024];
	float load;

	FileHandler = open("/proc/loadavg", O_RDONLY);
	if(FileHandler < 0) {
		return -1; }
	read(FileHandler, FileBuffer, sizeof(FileBuffer) - 1);
	sscanf(FileBuffer, "%f", &load);
	close(FileHandler);
	return (int)(load * 100);
}



void publish(MQTTClient client, char* topic, char* payload) {
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    pubmsg.payload = payload;
    pubmsg.payloadlen = strlen(pubmsg.payload);
    pubmsg.qos = 2;
    pubmsg.retained = 0;
    MQTTClient_deliveryToken token;
    MQTTClient_publishMessage(client, topic, &pubmsg, &token);
    MQTTClient_waitForCompletion(client, token, 1000L);
    printf(PRIORITY"Message '%s' with delivery token %d delivered\n", payload, token);
}

int on_message(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    char* payload = message->payload;
    printf("Received operation %s\n", payload);
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

int main(int argc, char* argv[]) {
    MQTTClient client;
    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

          
    conn_opts.username = "sepehr";
    conn_opts.password = "sepehr";

    MQTTClient_setCallbacks(client, NULL, NULL, on_message, NULL);

    int rc;
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS) {
        printf(PRIORITY"Failed to connect, return code %d\n", rc);
        exit(-1);
    }
 
    //listen for operation
    MQTTClient_subscribe(client, "s/ds", 0);
    

    int cpu_load;
	
	
    for (;;) {
		//send temperature measurement and CPU load
		char status[50];
		cpu_load = GetCPULoad();
		
		if(cpu_load < 90){
			sprintf(status, "CPU Load = %d%%", cpu_load);
			publish(client, "CPU/status", status);
		}

		else{
			sprintf(status, "CPU Load = %d%% and Temperature = 400", cpu_load);
			publish(client, "CPU/status", status);

		}


		sleep(20);
    }
    MQTTClient_disconnect(client, 1000);
    MQTTClient_destroy(&client);
    return rc;
}


