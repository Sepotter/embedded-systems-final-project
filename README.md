********************************************************
**                  Sepehr Dehdashtian                **
**                       97205893                     **
**          Embedded Class 2020: Final Project        **
**             Sepehr.dehdashtian@gmail.com           **
********************************************************

This is the embedded systems class 2020 final project.
The project prompt is as following:

![](proj.PNG)



********************************************************
**                       V4L                          **
********************************************************
- Install v4l2:

    `sudo apt install fswebcam libv4l-dev v4l-utils view libav-tools`



- Compile codes which include v4l2 library:

    `gcc code.c -o code -lv4l2`


- Compile codes using Qt Creator:

    Add this line to the CMakeList.txt:
    `topic_link_libraries(${PROJECT_NAME} -lv4l2)`



********************************************************
**                        CGI                         **
********************************************************
In order to create a web server to adjust the parameters of the camera, we need to pass user's inputs from an HTML page to a C code. To do so, we use CGI.

- Enable CGI:

    1. Enable CGI for Apache:

        `sudo a2enmod cgi`

    2. Restart Apache:
        `sudo service apache2 restart`
        or
        `sudo systemctl restart apache2`

- Use CGI:

    1. Place your executable file in `/usr/lib/cgi-bin/` directory.


    2. The script will require system executable permissions: 
	
	`chmod +x /usr/lib/cgi-bin/<Exe file>`

	and

	`sudo chmod 777 /dev/video0` Ref: [https://forums.zoneminder.com/viewtopic.php?t=23361](url)

	and

	`sudo chmod 777 /dev/video*`

	and

	`sudo chmod 777 /usr/lib/cgi-bin` if the code wants to create a directory or save something in this directory.
	


    3. In the browser, enter the following url:

        `localhost/cgi-bin/<YOUR EXE FILE NAME>`



- Error log:
	
	The error logs can be found in `/var/log/apache2/error.log`





********************************************************
**              Enabling WebCam in VMWare             **
********************************************************
To enable webcam in VMWare, in your host (windows) go to `Device Manager` then find the camera driver. Right click on it and click on `Scan for Hardware change`.
In the pop up window, select your VM and confirm it.




********************************************************
**               Restful HTTP server                  **
********************************************************
The codes and executable files of this part came in `3.Restful/` directory. The service file of the restful server is in `3.Restful/Services`. This service will listen on port `8081` with `100` workers and send back the topic file which is requested by client.
Note that the directory of the executable file in the service is hard-coded and may need to be changed in order to run on another machine.

- Usage:
	1. Move the `REST-server.service` file to `/etc/systemd/system`.

	2. In the terminal window, enter this command:
		
		`sudo systemctl enable REST-server.service`

	3. Open another terminal window in the `3.Restful/Client/` directory and enter the following command:

		`./restClient 0.0.0.0 8081 /<topic file> | tail -n +7 > <received file>`
		
		This command sends a request to get the <topic file> and saves it with <received file> name (First 7 lines of the response are logs of connection so we omit them).  



********************************************************
**                 Get CPU Load in C                  **
********************************************************
The load of CPU is written in `/proc/loadavg` file, so we need to load that file and read the related number.
The reference of the related function:  [https://www.raspberrypi.org/forums/viewtopic.php?t=64835](url)




********************************************************
**                       MQTT                         **
********************************************************
- Make the connection secure:

	1. Use command `mosquitto_passwd -c <Password filename> <username>` to create a new password file.

	2. Use command `mosquitto_passwd -b <Password filename> <username> <password>` in order to add new user.	
	
	3. Move your created password file to `/etc/mosquito `.

	4. Write the password file name and directory in `/etc/mosquito/conf.d/default.conf`:

		Add this line in the mentioned file : `password_file /etc/mosquitto/<Password filename>`

- Installing Paho:
	
	1. `git clone https://github.com/eclipse/paho.mqtt.c.git`

	2. `cd org.eclipse.paho.mqtt.c.git`
	
	3. `sudo apt-get install libssl-dev`
	
	4. `make`

	5. `sudo make install`
	
	6. `sudo make html`

	7. `apt-get install doxygen graphviz`


- Compiling codes:
	
	`gcc <code file name> -o <executable file name> -lpaho-mqtt3c`



- Codes:

	1. Publisher:

		C code of the publisher is in `1.MQTT/Publisher/publisher.c`. It publises the status of the system every 20 seconds to `CPU/status` topic. The format of the mentioned message is `CPU Load` in
		normal load of CPU and `CPU Load and Temperature` when the CPU load is over 90%.

	2. Subscriber:
		
		C code of the publisher is in `1.MQTT/Subscriber/subscriber.c` and `1.MQTT/Subscriber/callback.h`. This code writes received data from topic `CPU/status` to a text file called `logfile.txt`. This service is in `1.MQTT/Services/`.
		In order to show the received data, a PHP code is provided which displays the data from `logfile.txt`.
		

- Create service:

	1. Publisher:
		
		We need a service to run the executable file of the publisher automatically which publishes CPU load every 20 seconds. This service is in `1.MQTT/Services/`.

	2. Subscriber:
		  
		This service runs the executable file of the subscriber which writes received data from topic `CPU/status` to a text file called `logfile.txt`. This service is in `1.MQTT/Services/`.

	3. Broker:
	
		This service will be run on the broker side. It will enable the mossquito. This service is in `1.MQTT/Services/`.
********************************************************
**                   VLC Stream                       **
********************************************************

:sout=#transcode{vcodec=theo,vb=800,scale=Auto,acodec=vorb,ab=128,channels=2,samplerate=44100,scodec=none}:http{mux=ogg,dst=:8080/stream} :sout-all :sout-keep

********************************************************
**                   Apache Server                    **
********************************************************

- Install: 

	`sudo apt update && sudo apt upgrade -y`
	
	`sudo apt install apache2 -y`


********************************************************
**                       PHP                          **
********************************************************
- Install  

	1. `sudo apt install php -y`

	2. `sudo systemctl restart apache2`




********************************************************
**        MySQL (MariaDB) and PHP Complement          **
********************************************************
- Install 


	1. `sudo apt install mariadb-server php-mysql -y`

	2. `sudo systemctl restart apache2`

	3. `sudo mysql_secure_installation`

	4. Press “Enter” to confirm your current root password (which is none or blank).
	   Enter “Y” and press “Enter” again to set the root password.
           Type in a password at the `New Password` prompt, then press Enter. `systemd-analyze dot --to-pattern='*.topic' --from-pattern='*.topic' | dot -Tsvg >topics.svg`
 	   Type “Y” to remove anonymous users.
	   Type “Y” to disallow root login remotely.
	   Type “Y” to remove the test database and access to it.
	   Type “Y” to reload privilege tables now.

- How to use in Terminal:

	1. sudo mysql
	
	2. Create your first database

		`CREATE DATABASE test;`

	3. Create the first user

		`CREATE USER 'webuser' IDENTIFIED BY 'password';`

	4. Add all privileges to our test database to this user

		`GRANT ALL PRIVILEGES ON test.* To 'webuser'@'localhost' IDENTIFIED BY 'password';`

	5. Save the changes

		`FLUSH PRIVILEGES;`	

	6. Quit the MySQL console
		
		`quit`

********************************************************
**                        Alsa                        **
********************************************************
- Install:

	`sudo apt update`
	
	`sudo apt install mplayer alsa-utils libav-tools`


- list the available playback devices:
	
	`cat /proc/asound/pcm`

	`aplay -l`

- The ALSA utilities also provide you with detailed information about the 
 capabilities of a USB device. For example, amixer can be used to get and set
 an adapter’s available properties. Using amixer on the Sound Blaster device
 provides its current state information: (-c specifies the card number)
 
	 `amixer -c 1`

	 `amixer -c 1 controls`

	 `amixer -c 1 cset iface=MIXER,name='Speaker Playback Volume' 10,10`

- Playback
 
	 `cd .....chp15/audio`

	 `mplayer -ao alsa:device=hw=1 320sample.mp3`

	 `mplayer -ao alsa:device=hw=2 320sample.mp3`

	 `aplay -D plughw:1,0 cheering.wav`

	 `aplay -D plughw:2,0 cheering.wav`



********************************************************
**                                                    **
********************************************************

